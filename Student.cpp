#include "Student.h"
#include"Course.h"
#include <string>
using namespace std;

void Student::init(string name, Course** courses, int crsCount)
{
	this->_name = name;
	this->_courseList = courses;
	this->_crsCount = crsCount;
}

std::string Student::getName()
{
	return this->_name;
}

void Student::setName(string name)
{
	this->_name = name;
}


double Student::getAvg()
{
	int i = 0;
	double sum = 0;
	for ( i = 0; i < this->_crsCount; i++)
	{
		sum += (this->_courseList[i])->getFinalGrade();
	}
	return sum / this->_crsCount;
}

int Student::getCrsCount()
{
	return this->_crsCount;
}

Course** Student::getCourses()
{
	return this->_courseList;
}

