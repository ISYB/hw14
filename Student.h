#pragma once
#include "Course.h"
#include <string>
using namespace std;
class Student
{
public:
	void init(string name, Course** courses, int crsCount);
	std::string getName();
	void setName(string name);
	double getAvg();
	int getCrsCount();
	Course** getCourses();

private:
	Course ** _courseList;
	string _name;
	int _crsCount;

};

