#include "Course.h"
#include <iostream>
#include <string>


void Course::init(string name, int test1, int test2, int exam)
{
	this->name = name;
	this->exam = exam;
	this->test1 = test1;
	this->test2 = test2;
}

std::string Course::getName()
{
	return this->name;
}

unsigned int * Course::getGrades()
{
	unsigned int* arr = new unsigned int[3];
	arr[0] = this->test1;
	arr[1] = this->test2;
	arr[2] = this->exam;
	return arr;
}

double Course::getFinalGrade()
{
	return ((this->test1 + this->test2) / 2 + this->exam) /2 ;
}
