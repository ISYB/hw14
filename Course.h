#pragma once
#include <string>
using namespace std;
class Course
{
public:
	void init(string name, int test1, int test2, int exam);
	std::string getName();
	unsigned int * getGrades();
	double getFinalGrade();
	

private:
	std::string name;
	int test1;
	int test2;
	int exam;
};

